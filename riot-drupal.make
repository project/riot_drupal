api = 2
core =7.x

projects[drupal][type] = "core"
projects[drupal][version] = "7.22"

; Support the concept of sub-profiles
projects[drupal][patch][1815700] = "http://drupal.org/files/1356276-base-profile-d7-39-do-not-test.patch"

; Redirect to install.php
projects[drupal][patch][728702] = "http://drupal.org/files/issues/install-redirect-on-empty-database-728702-36.patch"
